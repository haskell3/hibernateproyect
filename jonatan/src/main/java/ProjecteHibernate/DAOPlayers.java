package ProjecteHibernate;

public class DAOPlayers extends DAOGeneric<Players, Integer>{

	public DAOPlayers() {
		super(Players.class);
	}
	
	public Players generatePlayer(String name, int wins) {
		Players p = new Players(name, wins);
		this.save(p);
		return p;
	}

}
