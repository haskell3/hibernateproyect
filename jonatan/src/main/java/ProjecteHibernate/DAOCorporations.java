package ProjecteHibernate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DAOCorporations extends DAOGeneric<Corporations, Integer> {

	public DAOCorporations() {
		super(Corporations.class);
	}

	public Corporations generateCorporations(String name, String descripcio, int victorypoints) {
		Corporations c = new Corporations(name, descripcio, victorypoints);
		this.save(c);
		return c;
	}

	public void setVictoryPointsMakers() {
		System.out.println("Setejan els punts de victoria segons el nombre de caselles");
		List<Corporations> corpo = this.findAll();
		for (Corporations c : corpo) {
			c.setVictorypoints(c.getVictorypoints() + c.getMakers().size());
			update(c);
		}
	}

	public void setExtraVictoryPointsMakers() {
		System.out.println("Setejan els punts de victoria extres");
		
		List<Corporations> corpo = this.findAll();
		List<Integer> markers;
		
		Corporations ci, bo,ci2, bo2;
		ci = bo = ci2 = bo2 = null;
		
		int cim, bom, cim2, bom2;
		cim = bom = cim2 = bom2 = 0;
		
		for (Corporations c : corpo) {
			markers = howManyOceaCiutatBosc(c);

			//ocea
			if(markers.get(0) >= 3) c.setVictorypoints(c.getVictorypoints() + 3);
			
			//ciutat
			if(markers.get(1) > cim) {
				cim = markers.get(1);
				if(ci != null) 
					ci2 = ci;
				ci = c;
			}else if(markers.get(1) == cim) {
				if(ci != null) {
					if(c.getId() < ci.getId()) {
						ci = c;
					}
				}else
					ci = c;
			}else if(markers.get(1) > cim2) {
				cim2 = markers.get(1);
				ci2 = c;
			}else if(markers.get(1) == cim2) {
				if(ci2 != null) {
					if(c.getId() < ci2.getId()) {
						ci2 = c;
					}
				}else
					ci2 = c;
			}		
			//bosc
			if(markers.get(2) > bom) {
				bom = markers.get(2);
				if(bo != null) 
					bo2 = bo;
				bo = c;
			}else if(markers.get(2) == bom) {
				if(bo != null) {
					if(c.getId() < bo.getId()) {
						bo = c;
					}
				}else
					bo = c;
			}else if(markers.get(2) > bom2) {
				bom2 = markers.get(2);
				bo2 = c;
			}else if(markers.get(2) == bom2) {
				if(bo2 != null) {
					if(c.getId() < bo2.getId()) {
						bo2 = c;
					}
				}else
					bo2 = c;
			}
		}
		if(ci != null)
			ci.setVictorypoints(ci.getVictorypoints() + 5);
		if(bo != null)
			bo.setVictorypoints(ci.getVictorypoints() + 5);
		
		if(ci2 != null)
			ci2.setVictorypoints(ci.getVictorypoints() + 3);
		if(bo2 != null)
			bo2.setVictorypoints(ci.getVictorypoints() + 3);
		
		for (Corporations c : corpo) {
			update(c);
		}
		
	}

	private List<Integer> howManyOceaCiutatBosc(Corporations c) {
		List<Integer> in = new ArrayList<Integer>();
		int oc, bo, ci;
		oc = bo = ci = 0;
		for (Makers m : c.getMakers()) {
			if(m.getTypemaker() == TypeMakers.oceà)
				oc++;
			else if(m.getTypemaker() == TypeMakers.ciutat)
				bo++;
			else if(m.getTypemaker() == TypeMakers.bosc)
				ci++;
		}
		in.add(oc);
		in.add(ci);
		in.add(bo);
		return in;
	}
	
	public void setWinnerGame(Games g, DAOGames dm, DAOPlayers dp) {
		List<Corporations> corpo = this.findAll();
		int punts = 0;
		Corporations winner = null;
		for (Corporations c : corpo) {
			if (punts < c.getVictorypoints()) {
				punts = c.getVictorypoints();
				winner = c;
			}else if(punts == c.getVictorypoints()){
				if(winner != null) {
					if(c.getId() < winner.getId()) winner = c;					
				}
				else winner = c;
			}
		}
		g.setWinPlayer(winner.getPlayers());
		g.setDateend(LocalDateTime.now());
		winner.getPlayers().setWins(winner.getPlayers().getWins() + 1);
		
		dm.update(g);
		dp.update(winner.getPlayers());
		
		System.out.println("I el guanyador es....");
		System.out.println(winner);
	}

}
