package ProjecteHibernate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "corporations")
public class Corporations {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idcorporations")
	private int id;
	@Column(name = "Name", length = 50, nullable = false)
	private String name;
	@Column(name = "descripcio", length = 50, nullable = false)
	private String descripcio;
	@Column(name = "Victorypoints")
	private int victorypoints = 0;
	@OneToMany(mappedBy = "corporations", fetch = FetchType.EAGER)
	private Set<Makers> makers = new HashSet<Makers>();
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_players", unique = true)
	private Players players;

	public Corporations() {
		super();
	}
	public Corporations(String name, String descripcio, int victorypoints) {
		this.name = name;
		this.descripcio = descripcio;
		this.victorypoints = victorypoints;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public int getVictorypoints() {
		return victorypoints;
	}

	public void setVictorypoints(int victorypoints) {
		this.victorypoints = victorypoints;
	}

	public Set<Makers> getMakers() {
		return makers;
	}

	public void setMakers(Set<Makers> makers) {
		this.makers = makers;
	}

	public Players getPlayers() {
		return players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "Corporations [id=" + id + ", name=" + name + ", descripcio=" + descripcio + ", victorypoints="
				+ victorypoints + ", players=" + players + "]";
	}

}
