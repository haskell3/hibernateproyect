package ProjecteHibernate;

import java.time.LocalDateTime;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		DAOPlayers daoPlayer = new DAOPlayers();
		DAOGames daoGames = new DAOGames();
		DAOCorporations daoCorporations = new DAOCorporations();
		DAOMakers daoMakers = new DAOMakers();
		
		Players[] p = new Players[] {daoPlayer.generatePlayer("Azunya", 0), daoPlayer.generatePlayer("Ari", 0), daoPlayer.generatePlayer("Gisela", 0), daoPlayer.generatePlayer("Almaterial", 0)};

		Games g = daoGames.generateGames(0, -30, 0);
		Corporations[] c = new Corporations[4];
		Random r = new Random();
		for (int i = 0; i < c.length; i++) {
			c[i] = daoCorporations.generateCorporations(nameCorporations.values()[r.nextInt(nameCorporations.values().length)].toString(), "Another Corporation", 0);
			c[i].setPlayers(p[i]);
			daoCorporations.update(c[i]);
		}
		for (int i = 0; i < 61; i++) {
			if(i > 23)
				daoMakers.generateMakers(i + "", 6, TypeMakers.values()[r.nextInt(TypeMakers.values().length)]);
			else if(i > 5)
				daoMakers.generateMakers(i + "", 4, TypeMakers.values()[r.nextInt(TypeMakers.values().length)]);
			else
				daoMakers.generateMakers(i + "", 3, TypeMakers.values()[r.nextInt(TypeMakers.values().length)]);
		}
		for (int i = 0; i < p.length; i++) {
			p[i].getGames().add(g);
			daoPlayer.update(p[i]);			
		}
		
		daoMakers.setVeins();
		
		System.out.println("Comença el joc");
		int torn = 0;
		while(!daoGames.isEndedGame(g, daoMakers)) {
			daoGames.resolveDices(c[torn], daoGames.rollingDices(), g, daoMakers, daoCorporations);
			torn++;
			if(torn >= 4)
				torn = 0;
		}
		daoCorporations.setVictoryPointsMakers();
		daoCorporations.setExtraVictoryPointsMakers();
		daoCorporations.setWinnerGame(g, daoGames, daoPlayer);
		
		System.out.println("Fi del joc");
		Utils.close();
	}

	

}

enum nameCorporations{
	CREDICOR, ECOLINE, HELION, MINING_GUILD, PHOBLOG, THARSIS_REPUBLIC, THORGATE, U_N_MARS_INITIATIVE
}
