package ProjecteHibernate;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "games")
public class Games {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idgame")
	private int id;
	@Column(name = "Oxygen", columnDefinition = "double(6,2)")
	private double oxygen = 00.00;
	@Column(name = "Temperature", columnDefinition = "double(4,2)")
	private double temperature = 00.00;
	@Column(name = "Oceans")
	private int oceans = 0;
	@Column(name = "Datestart")
	private LocalDateTime datestart = LocalDateTime.now();
	@Column(name = "Dateend")
	private LocalDateTime dateend;
	@ManyToMany(mappedBy = "games")
	private Set<Players> players = new HashSet<Players>();
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_players")
	private Players WinPlayer;

	public Games(double oxygen, double temperature, int oceans, LocalDateTime datestart, LocalDateTime dateend) {
		this.oxygen = oxygen;
		this.temperature = temperature;
		this.oceans = oceans;
		this.datestart = datestart;
		this.dateend = dateend;
	}

	public Games(double oxygen, double temperature, int oceans) {
		super();
		this.oxygen = oxygen;
		this.temperature = temperature;
		this.oceans = oceans;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getOxygen() {
		return oxygen;
	}

	public void setOxygen(double oxygen) {
		this.oxygen = oxygen;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public int getOceans() {
		return oceans;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}

	public LocalDateTime getDatestart() {
		return datestart;
	}

	public void setDatestart(LocalDateTime datestart) {
		this.datestart = datestart;
	}

	public LocalDateTime getDateend() {
		return dateend;
	}

	public void setDateend(LocalDateTime dateend) {
		this.dateend = dateend;
	}

	public Set<Players> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Players> players) {
		this.players = players;
	}

	public Players getWinPlayer() {
		return WinPlayer;
	}

	public Games() {
		super();
	}

	public void setWinPlayer(Players winPlayer) {
		WinPlayer = winPlayer;
	}

	@Override
	public String toString() {
		return "Games [id=" + id + ", oxygen=" + oxygen + ", temperature=" + temperature + ", Oceans=" + oceans
				+ ", datestart=" + datestart + ", dateend=" + dateend + "]";
	}

}
