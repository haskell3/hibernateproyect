package ProjecteHibernate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Players {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idplayer")
	private int id;
	@Column(name = "Name", length = 50, nullable = false)
	private String name;
	@Column(name = "Wins")
	private int wins = 0;
	@OneToOne(mappedBy = "players")
	private Corporations corporationsPlayer;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Player_games", joinColumns = @JoinColumn(name = "id_player"), inverseJoinColumns = @JoinColumn(name = "id_game"))
	private Set<Games> games = new HashSet<Games>();
	@OneToMany(mappedBy = "WinPlayer", fetch = FetchType.EAGER)
	private Set<Games> winGame = new HashSet<Games>();

	public Players(String name, int wins) {
		this.name = name;
		this.wins = wins;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Players() {
		super();
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public Corporations getCorporationsPlayer() {
		return corporationsPlayer;
	}

	public void setCorporationsPlayer(Corporations corporationsPlayer) {
		this.corporationsPlayer = corporationsPlayer;
	}

	public Set<Games> getGames() {
		return games;
	}

	public void setGames(Set<Games> games) {
		this.games = games;
	}

	public Set<Games> getWinGame() {
		return winGame;
	}

	public void setWinGame(Set<Games> winGame) {
		this.winGame = winGame;
	}

	@Override
	public String toString() {
		return "Players [id=" + id + ", name=" + name + ", wins=" + wins + "]";
	}

}
