package ProjecteHibernate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "makers")
public class Makers {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idmakers")
	private int id;
	@Column(name = "Name", length = 50, nullable = false)
	private String name;
	@Column(name = "Maxneighbours")
	private int maxneighbours = 0;
	@Column(name = "Typemaker", columnDefinition = "double(6,2)")
	private TypeMakers typemaker;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "veins", joinColumns = @JoinColumn(name = "id_Makers1"), inverseJoinColumns = @JoinColumn(name = "id_makers2"))
	private Set<Makers> veins = new HashSet<Makers>();
	@ManyToMany(mappedBy = "veins", fetch = FetchType.EAGER)
	private Set<Makers> veiDe = new HashSet<Makers>();
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_corporations")
	private Corporations corporations;

	public Makers(String name, int maxneighbours, TypeMakers typemaker) {
		this.name = name;
		this.maxneighbours = maxneighbours;
		this.typemaker = typemaker;
	}

	public Set<Makers> getVeiDe() {
		return veiDe;
	}
	
	public void setVeiDe(Set<Makers> veiDe) {
		this.veiDe = veiDe;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxneighbours() {
		return maxneighbours;
	}

	public void setMaxneighbours(int maxneighbours) {
		this.maxneighbours = maxneighbours;
	}

	public TypeMakers getTypemaker() {
		return typemaker;
	}

	public void setTypemaker(TypeMakers typemaker) {
		this.typemaker = typemaker;
	}

	public Set<Makers> getVeins() {
		return veins;
	}

	public void setVeins(Set<Makers> veins) {
		if (this.maxneighbours > this.getVeins().size() + veins.size()) {
			this.veins = veins;
		}
	}

	public void addvei(Makers vei) {
		if (this.maxneighbours > (this.getVeins().size() + this.getVeiDe().size())) {
			System.out.println("Id: " + this.getId() + "| Nou vei: " + ((vei != null) ? vei.getId() : null));
			this.veins.add(vei);
		}
	}

	public Makers() {
		super();
	}

	public Corporations getCorporations() {
		return corporations;
	}

	public void setCorporations(Corporations corporations) {
		this.corporations = corporations;
	}

	@Override
	public String toString() {
		return "Makers [id=" + id + ", name=" + name + ", maxneighbours=" + maxneighbours + ", typemaker=" + typemaker
				+ ", corporations=" + corporations + "]";
	}

}
