package ProjecteHibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DAOGames extends DAOGeneric<Games, Integer> {
	
	public DAOGames() {
		super(Games.class);
	}

	public Games generateGames(double oxygen, double temperature, int oceans) {
		Games g = new Games(oxygen, temperature, oceans);
		this.save(g);
		return g; 
	}

	public List<Integer> rollingDices() {
		ArrayList<Integer> dices = new ArrayList<Integer>();
		Random r = new Random();
		for (int i = 0; i < 6; i++) {
			int x = r.nextInt(6) + 1;
			dices.add(x);
		}
		return dices;
	}
	
	public void resolveDices(Corporations c, List<Integer> dices, Games g, DAOMakers dm, DAOCorporations dc) {
		System.out.println("Resolen la tirada del jugador: ");
		System.out.println(c.getPlayers());
		System.out.println("Els seus daus son: ");
		System.out.println(dices);
		int temp, ox, vp, oc, ci, bos;
		temp = ox = vp = oc = ci = bos = 0;
		for (Integer integer : dices) {
			switch (integer) {
			case 1: 
				temp++;
				break;
			case 2: 
				ox++;
				break;
			case 3: 
				oc++;
				break;
			case 4: 
				bos++;
				break;
			case 5: 
				ci++;
				break;
			case 6: 
				vp++;
				break;
			default:
				break;
			}
		}
		while(temp >= 3) {
			g.setTemperature(g.getTemperature() + 2);
			temp -= 3;
		}
		while(ox >= 3) {
			g.setOxygen(g.getOxygen() + 1);
			ox -= 3;
		}
		while(vp >= 3) {
			c.setVictorypoints(c.getVictorypoints() + 2);
			vp -= 3;
		}
		Makers modelo;
		List<Makers> lista;
		while(oc >= 3) {
			lista = dm.getMakersByTypeNoCorporation(TypeMakers.oceà);
			if(lista.size() > 0) {
				modelo = lista.get(0);
				modelo.setCorporations(c);
				dm.update(modelo);
			}else
				System.out.println("No hi han més caselles d'oceà :(");
			oc -= 3;
		}
		while(ci >= 3) {
			lista = dm.getMakersByTypeNoCorporation(TypeMakers.ciutat);
			if(lista.size() > 0) {
				modelo = lista.get(0);
				modelo.setCorporations(c);
				dm.update(modelo);
			}else 
				System.out.println("No hi han més caselles de ciutat :(");
			ci -= 3;
		}
		if(bos >= 4){
			lista = dm.getMakersByTypeNoCorporation(TypeMakers.bosc);
			if(lista.size() > 0) {
				modelo = lista.get(0);
				modelo.setCorporations(c);
				dm.update(modelo);
			}else
				System.out.println("No hi han més caselles de bosc :(");
		}
		dc.update(c);
		update(g);
		
	}
	
	public boolean isEndedGame(Games g, DAOMakers dm) {
		System.out.println();
		System.out.println("Comprovant si s'ha acabat el joc");
		int cont = 0;
		if(g.getTemperature() >= 0) cont++;
		if(g.getOxygen() >= 14) cont++;
		if(dm.areOceansOccupied()) cont++;
		System.out.println("----Temperatura-----");
		System.out.println(g.getTemperature());
		System.out.println("----Oxigen-----");
		System.out.println(g.getOxygen());
		System.out.println("Oceans ocupats?");
		System.out.println(dm.areOceansOccupied());
		System.out.println();
		return cont >= 2;
	}
	
	

}
