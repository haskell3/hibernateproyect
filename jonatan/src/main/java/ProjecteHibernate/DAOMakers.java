package ProjecteHibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DAOMakers extends DAOGeneric<Makers, Integer> {

	public DAOMakers() {
		super(Makers.class);
	}

	public Makers generateMakers(String name, int maxneighbours, TypeMakers typemaker) {
		Makers m = new Makers(name, maxneighbours, typemaker);
		this.save(m);
		return m;
	}

	public List<Makers> getMakersByType(TypeMakers typemaker) {
		List<Makers> makers = this.findAll();
		List<Makers> byType = new ArrayList<Makers>();
		for (Makers m : makers) {
			if (typemaker == m.getTypemaker()) {
				byType.add(m);
			}
		}
		return byType;
	}
	
	public boolean areOceansOccupied() {
		List<Makers> makers = getMakersByType(TypeMakers.oceà);
		for (Makers m : makers) {
			if (m.getCorporations() == null) {
				return false;
			}
		}
		return true;
	}

	public List<Makers> getMakersByTypeNoCorporation(TypeMakers typem) {
		List<Makers> makers = this.findAll();
		List<Makers> byType = new ArrayList<Makers>();
		for (Makers m : makers) {
			if (typem == m.getTypemaker() && m.getCorporations() == null) {
				byType.add(m);
			}
		}
		return byType;
	}

	public List<Makers> getMakersByTypeCorporation(TypeMakers typem) {
		List<Makers> makers = this.findAll();
		List<Makers> byType = new ArrayList<Makers>();
		for (Makers m : makers) {
			if (typem == m.getTypemaker() && m.getCorporations() != null) {
				byType.add(m);
			}
		}
		return byType;
	}

	public Makers getRandomMakerNoNeighbours() {
		Random r = new Random();
		int x;;
		Makers m;
		if(!canNeighbour()) return null;
		do {
			x = r.nextInt(this.findAll().size()) + 1;
			m = find(x);
		}while(!(m.getMaxneighbours() > m.getVeins().size()));
		return m;
	}

	public boolean canNeighbour() {
		for(Makers m : findAll()) {
			System.out.println("Id: " + m.getId() + " Maximos: " + m.getMaxneighbours() + " Vecinos: " +  m.getVeins().size());
			if(m.getMaxneighbours() > m.getVeins().size())
				return true;
		}
		return false;
	}

	public List<Makers> getAllMakerPossibleNeighbours() {
		List<Makers> makers = this.findAll();
		List<Makers> byType = new ArrayList<Makers>();
		for (Makers m : makers) {
			if (m.getMaxneighbours() > m.getVeins().size()) {
				byType.add(m);
			}
		}
		return byType;
	}
	
	public void addMakerPossibleNeighbours(Makers m1, Makers m2) {
		if (m1.getId() == m2.getId()) return;
		if (!bothAvaible(m1, m2)) return;
		if (areNeighbours(m1, m2)) return;	
		m1.addvei(m2);
		update(m1);
	}

	private boolean areNeighbours(Makers m1, Makers m2) {
		for (Makers m : m1.getVeins()) {
			if(m.getId() == m2.getId()) return true;
		}
		for (Makers m : m2.getVeins()) {
			if(m.getId() == m1.getId()) return true;
		}
		return false;
	}

	private boolean bothAvaible(Makers m1, Makers m2) {
		return m1.getMaxneighbours() > m1.getVeins().size() + m1.getVeiDe().size() && m2.getMaxneighbours() > m2.getVeins().size() + m2.getVeiDe().size();
	}

	public void setVeins() {
			ArrayList<Makers> makersM3 = (ArrayList<Makers>) makersMaxNeighbours(3);
			for (int i = 0; i < makersM3.size(); i++) {
				if(i<makersM3.size()-1) 
					addMakerPossibleNeighbours(makersM3.get(i), makersM3.get(i + 1));
			}
			
			ArrayList<Makers> makersM4 = (ArrayList<Makers>) makersMaxNeighbours(4);
			
			for (int i = 0; i < makersM4.size(); i++) {
				if(i<makersM4.size()-1) 
					addMakerPossibleNeighbours(makersM4.get(i), makersM4.get(i + 1));
			}
			
			ArrayList<Makers> makersM6 = (ArrayList<Makers>) makersMaxNeighbours(6);
			System.out.println(makersM6);
			for (int i = 0; i < makersM6.size(); i++) {
				System.out.println(i);
				if(i<makersM6.size()-1) 
					addMakerPossibleNeighbours(makersM6.get(i), makersM6.get(i + 1));
			}
			
	}
	private List<Makers> makersMaxNeighbours(int veins){
		
		List<Makers> makers = new ArrayList<Makers>();
		
		for (Makers makers2 : findAll()) {
			if(makers2.getMaxneighbours() == veins) {
				if(makers2.getVeins().size() + makers2.getVeiDe().size() < veins) {
					makers.add(makers2);					
				}
				
			}
		}
		return makers;
	}

	

	
}
